<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 12/22/2014
 * Time: 10:37 AM
 */

namespace ObservantRecords\WordPress\Themes\NemesisVex2020;


class Setup {

	public static function init() {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'wp_enqueue_styles'), 21);
	}

	public static function wp_enqueue_styles() {
		wp_dequeue_style( 'observantrecords2020-style' );

        wp_enqueue_style( 'nemesisvex2020-header-font', '//fonts.googleapis.com/css2?family=Special+Elite&display=swap' );
        wp_enqueue_style( 'nemesisvex2020-body-font', '//fonts.googleapis.com/css2?family=DM+Serif+Text:ital@0;1&display=swap' );
        wp_enqueue_style( 'nemesisvex2020-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );
	}

}