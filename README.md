# Nemesis Vex 2020 Theme

A custom theme for [NEMESIS VEX](http://nemesisvex.com/).

## Dependencies

* [Observant Records 2020 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2020-for-wordpress)
